function doIt() {
    let number1 = Number(document.getElementById("number1").value)
    let number2 = Number(document.getElementById("number2").value)
    let number3 = Number(document.getElementById("number3").value)
    let sign = document.getElementById("answer")
    let parity = document.getElementById("parity")

    let answer = number1 + number2 + number3

    if (answer < 0) {
        sign.className = "negative"
    } else {
        sign.className = "positive"
    }

    sign.innerHTML = answer

    if (answer % 2 === 0) {
        parity.innerHTML = "(Even)"
        parity.className = "even"
    } else {
        parity.innerHTML = "(Odd)"
        parity.className = "odd"
    }
}