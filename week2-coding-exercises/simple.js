//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1() {
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    let array = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25]
    let positiveOdd = []
    let negativeEven = []
    for (let i = 0; i < array.length; i++) {
        if (array[i] > 0 && array[i] % 2 === 1) {
            positiveOdd.push(array[i])
        }
    } for (let i = 0; i < array.length; i++){
        if (array[i] < 0 && array[i] % 2 === 0) {
            negativeEven.push(array[i])
        }
    }
    output += `Positive Odd: ${positiveOdd}\n`
    output += `Negative Even: ${negativeEven}`
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2() {
    let output = "" 
    
    //Question 2 here 
    let d1 = 0, d2 = 0, d3 = 0, d4 = 0, d5 = 0, d6 = 0
    let dice = 0
    for (let i = 0; i < 60000; i++) {
        let dice = Math.floor((Math.random() * 6) + 1)
        if (dice === 1) {
            d1++
        } else if (dice === 2) {
            d2++
        } else if (dice === 3) {
            d3++
        } else if (dice === 4) {
            d4++
        } else if (dice === 5) {
            d5++
        } else if (dice === 6) {
            d6++
        }
    }
    output += `1: ${d1}\n`
    output += `2: ${d2}\n`
    output += `3: ${d3}\n`
    output += `4: ${d4}\n`
    output += `5: ${d5}\n`
    output += `6: ${d6}`
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3() {
    let output = "" 
    
    //Question 3 here 
    let d = [1, 2, 3, 4, 5, 6, 7]
    for (let i = 0; i < 60000; i++){
        let dice = Math.floor((Math.random() * 6) + 1)
        d[dice]++
    }
    output += `1: ${d[1]}\n`
    output += `2: ${d[2]}\n`
    output += `3: ${d[3]}\n`
    output += `4: ${d[4]}\n`
    output += `5: ${d[5]}\n`
    output += `6: ${d[6]}`
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4() {
    let output = "" 
    
    //Question 4 here 
    let dieRolls = {
        Frequencies: {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0,
        },
        Total: 60000,
        Exceptions: ""
    }
    for (let i = 0; i < 60000; i++){
        let dice = Math.floor((Math.random() * 6) + 1)
        dieRolls.Frequencies[dice]++
    }
    for (let prop in dieRolls.Frequencies) {
        if (dieRolls.Frequencies > 10100 || dieRolls.Frequencies < 9900) {
            dieRolls.Exceptions += prop
        }
    }
    output += `Frequency of dice rolls \n Total rolls: ${dieRolls.Total} \n 1: ${dieRolls.Frequencies[1]} \n 2: ${dieRolls.Frequencies[2]} \n 3: ${dieRolls.Frequencies[3]} \n 4: ${dieRolls.Frequencies[4]} \n 5: ${dieRolls.Frequencies[5]} \n 6: ${dieRolls.Frequencies[6]} \n Exceptions: ${dieRolls.Exceptions}`
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5() {
    let output = "" 
    
    //Question 5 here 
    let person = {
        name: "Jane",
        income: 127050
    }
    let tax = 0
    if (person.income <= 18200) {
        tax = 0
    } else if (person.income <= 37000) {
        tax = (person.income - 18200) * 0.19
    } else if (person.income <= 90000){
        tax = 3572 + (person.income - 37000) * 0.325
    } else if (person.income <= 180000) {
        tax = 20797 + (person.income - 90000) * 0.37
    } else if (person.income > 180000) {
        tax = 54097 + (person.income - 180000) * 0.45
    }
    output += `${person.name}'s income is: $${person.income}, and her tax owed is: $${tax}`
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}