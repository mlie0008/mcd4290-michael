question1()
question2()
question4()

function question1() {
    let output = ""
    let object = ""

    function  objectToHTML(){
        for (let prop in testObj) {
            object += `${prop}: ${testObj[prop]}\n`
        }
        return object
    }
    let testObj = {
        number: 1,
        string: "abc",
        array: [5, 4, 3, 2, 1],
        boolean: true
    }
    output += objectToHTML()

    let outPutArea = document.getElementById("outputArea1")
    outPutArea.innerText = output
}

function question2() {
    var outputAreaRef = document.getElementById("outputArea2");
    var output = "";

    output += flexible(addition, 3, 5) + "<br/>";
    output += flexible(multiplication, 3, 5) + "<br/>";

    outputAreaRef.innerHTML = output;
      
}
function multiplication(num1,num2) {
    return num1 * num2
}
function addition(num1, num2) {
    return num1 + num2
}
function flexible(fOperation, operand1, operand2) {
    var result = fOperation(operand1, operand2);
    return result;
}

/* question 3

Create array with variable "values"

Create a function "extremeValues"
    Create empty array with variable "array"
    Define minimum value with index = 0 from "values"
    Define maximum value with index = 0 from "values"
    for loop for minimum
        let variable "number" = "values" index
        if "number" is smaller than "values"
            replace current minimum value with value from "number"
    for loop for maximum
        let variable "number" = "values" index
        if "number" is larger than "values"
            replace current maximum value with value from "number"
    Push values of minimum and maximum to empty array "array"
    Return "array"
Output "extremeValues"
*/

function question4() {
    let output = ""

    let values = [4, 3, 6, 12, 1, 3, 8]

    function extremeValues() {
        let array = []
        let minimum = values[0]
        let maximum = values[0]
        for (let i = 1; i < values.length; i++) {
            let number = values[i]
            if (minimum > number) {
                minimum = number
            }
        } 
        for (let i = 1; i < values.length; i++) {
            let number = values[i]
            if (maximum < number) {
                maximum = number
            }
        } 
        array.push(minimum)
        array.push(maximum)
        return array
    }
    output += extremeValues()

    let outPutArea = document.getElementById("outputArea4")
    outPutArea.innerText = output
}