"use strict";

let bookingData = [{
    time: "08:00",
    reason: "",
    label: "",
    booked: false
}, {
    time: "09:00",
    reason: "",
    label: "",
    booked: false
}, {
    time: "10:00",
    reason: "",
    label: "",
    booked: false
}, {
    time: "11:00",
    reason: "",
    label: "",
    booked: false
}, {
    time: "12:00",
    reason: "",
    label: "", 
    booked: false
}, {
    time: "13:00",
    reason: "",
    label: "",
    booked: false
}, {
    time: "14:00",
    reason: "",
    label: "",
    booked: false
}, {
    time: "15:00",
    reason: "",
    label: "",
    booked: false
}, {
    time: "16:00",
    reason: "",
    label: "",
    booked: false
}, {
    time: "17:00",
    reason: "",
    label: "",
    booked: false
}
];

// Takes the index of array bookingData, inputReason, and inputLabel to modify the objects reason, label, and booked at a specified time.
function bookRoom(booking, inputReason, inputLabel) { 
    booking.reason = inputReason
    booking.label = inputLabel
    booking.booked = true
}

// Takes the index of array bookingData to check if objects reason or label are empty at a specified time. Returns true if room is available.
function checkRoomBooked(booking) { 
    if (booking.reason.length !== 0 || booking.label.length !== 0) {
        alert("This room hass already been booked!")
        return false
    }
    return true
}

// For loop to clear and reset all the values of the objects in array bookingData to default.
function clearRoomBookings() {
    for (let i = 0; i < bookingData.length; i++) {
        bookingData[i].reason = ""
        bookingData[i].label = ""
        bookingData[i].booked = false
    }
}

// Iterates over array bookingData to display the individual time and status of the room. Returns string output.
function updateDisplay() {
    let availability = ""
    let output = ""

    // For loop to check which status to show and the label for the time specified based on the contents of array bookingData. Pushes the strings to variable output.
    for (let i = 0; i < bookingData.length; i++) {
        if (bookingData[i].booked) {
            availability = "Not Available"
            output += `${bookingData[i].time}: ${availability} (${bookingData[i].label})<p>`
        } else {
            availability = "Available"
            output += `${bookingData[i].time}: ${availability}<p>`
        }
    }

    // Sets the value of the element with id output as variable output.
    document.getElementById("output").innerHTML = output
}

// Validation function that confirms with the user and takes the inputs inputTime, inputReason, and inputLabel and checks whether the values are empty.
function doBooking() {
    if (confirm("Are you sure you would like to make this booking?")) {
        // Takes the input inputTime and compares that with the object time from array bookingData to find the index.
        let inputTime = document.getElementById("inputTime").value
        let bookingDataIndex = bookingData.findIndex(x => x.time === inputTime)
        let booking = bookingData[bookingDataIndex]

        // Takes the input inputReason and inputLabel.
        let inputReason = document.getElementById("inputReason").value
        let inputLabel = document.getElementById("inputLabel").value
        
        // If statement to make sure that the user has selected an input time, and that the reason or length is not empty.
        if (inputTime.length === 0) {
            alert("Please select a time!")
            return false
        }
        if (inputReason.length === 0 || inputLabel.length === 0) {
            alert("Reason or label cannot be empty!")
            return false
        }

        // If function checkRoomBooked returns true, returns functions bookRoom() and updateDisplay().
        if (checkRoomBooked(booking)) {
            bookRoom(booking, inputReason, inputLabel)
            updateDisplay()
        }
    }
}

// Function that interacts with the DOM to confirm with the user and call the functions clearRoomBookings() and updateDisplay().
function clearAllBookings() {
    if (confirm("Are you sure you want to clear all bookings?\nWARNING: THIS ACTION CANNOT BE UNDONE")) {
        clearRoomBookings()
        updateDisplay()
    }
}

// Time function that takes the current time. Returns string.
function updateDayTime() {
    let date = new Date()
    let hours = date.getHours()
    let minutes = date.getMinutes()
    let seconds = date.getSeconds()

    // Ensures that the hours, minutes, and seconds that are < 10 have a leading zero.
    function zero(i) {
        if (i < 10) {
            i = "0" + i
        }
        return i
    }

    // If hours, minutes, or seconds is < 10, add a leading zero so that the time format is correct.
    hours = zero(hours)
    minutes = zero(minutes)
    seconds = zero(seconds)

    // Takes the variables hours, minutes, and seconds and outputs as a string to the element with id timeNow to output a working clock.
    document.getElementById("timeNow").innerHTML = `${hours}:${minutes}:${seconds}`
}

// Anonymous function that runs when the web page is loaded so that the times and the status of the room is shown as well as the clock. Calls function updateDayTime() and updateDisplay().
window.onload = function () {
    // Timer that calls the function updateDayTime repeatedly every 1000 milliseconds.
    updateDayTime()
    setInterval(updateDayTime, 1000)
    updateDisplay()
}