"use strict";

//michael_add.js connects with the onClick attribute from michael_add.html to add the student details from the user input and stores their details to the local storage.

// Function addStudent() takes the inputs values from HTML.
function addStudent() {
    let inputFullName = document.getElementById("fullName").value
    let inputStudentId = document.getElementById("studentId").value
    let inputProblem = document.getElementById("problem").value

    // Checks if all of the inputs are valid. Returns false if they are invalid.
    if (inputFullName < 1) {
        document.getElementById("fullName_msg").innerHTML = "Enter your name!"
        return false
    }
    if (inputStudentId < 1) {
        document.getElementById("studentId_msg").innerHTML = "Enter your student ID!"
        return false
    }
    if (inputProblem < 1) {
        document.getElementById("problem_msg").innerHTML = "Enter your problem!"
        return false
    }
    // Checks if the student ID format is valid.
    const regex = /^[1-3]{1}[0-9]{7}$/;
    inputStudentId.match(regex)
    document.getElementById("studentId_msg").innerHTML = "Enter a valid student ID"

    // Calls addStudent() method with the input values as it's parameters and saves them to local storage.
    consultSession.addStudent(inputFullName, Number(inputStudentId), inputProblem)
    updateLocalStorageData(APP_DATA_KEY, consultSession)

    // Alerts the user that they have been added to the queue and redirects them automatically to the main page.
    alert("You have been added to the queue. Now Redirecting...")
    window.location = "michael_index.html"
}