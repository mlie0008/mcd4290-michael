"use strict";

// michael_main.js has all the functions required to run michael_index.html including the live clock, and updating the web page to show the queue status to the user.

// Function liveClock() starts when the page is loaded and displays a live clock on the main page.
function liveClock() {
    let date = new Date()
    let hours = date.getHours()
    let minutes = date.getMinutes()
    let seconds = date.getSeconds()

    // Ensures that the hours, minutes, and seconds that are < 10 have a leading zero.
    function zero(i) {
        if (i < 10) {
            i = "0" + i
        }
        return i
    }

    // If hours, minutes, or seconds is < 10, add a leading zero so that the time format is correct.
    hours = zero(hours)
    minutes = zero(minutes)
    seconds = zero(seconds)

    // Takes the variables hours, minutes, and seconds and outputs as a string to the element with id currentTime to output a live clock.
    document.getElementById("currentTime").innerHTML = `${hours}:${minutes}:${seconds}`
}

// function view() takes the student index and queue index to find a specific student instance.
function view(studentIndex, queueIndex) {

    // Save the student and queue indexes to local storage and assigns the appropriate keys.
    localStorage.setItem(STUDENT_INDEX_KEY, studentIndex)
    localStorage.setItem(STUDENT_QUEUE_KEY, queueIndex)
    
    // Redirects the user to the view page.
    window.location = "michael_view.html"
}

// Function markDone() takes the index of the student data and its respective subqueue and marks them as done.
function markDone(studentIndex, queueIndex) {
    // Confirms with the user whether they want to mark this student as done.
    if (confirm("Are you sure you want to mark this student as done?")) {
        // Calls the removoeStudent() method from the session instance with the student data index and the respective subqueue as the parameters.
        consultSession.removeStudent(studentIndex, queueIndex)
        // Saves the new data to local storage.
        updateLocalStorageData(APP_DATA_KEY, consultSession)
    }
    // Calls function displayQueueStatus to update the main page.
    displayQueueStatus(consultSession)
}

// Function displayQueueStatus takes the session instance.
function displayQueueStatus(data) {
    let output = ""

    // Nested for loop to arrange the students into their respective subquques and pushes the HTML code to the main page.
    for (let i = 0; i < data._queue.length; i++) {
        let subQueue = data._queue[i]

        output += `<h4>Sub Queue ${i+1}</h4>`

        // Dynamically changes the onclick attributes.
        for (let j = 0; j < subQueue.length; j++) {
            let fullName = subQueue[j]._fullName

            output +=
            `<li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">
                    <span>${fullName}</span>
                </span>
                <span class="mdl-list__item-secondary-content">
                    <a class="mdl-list__item-secondary-action" onclick="view(${j}, ${i})"><i
                        class="material-icons">info</i>
                    </a>
                </span>
                <span class="mdl-list__item-secondary-content">
                    <a class="mdl-list__item-secondary-action" onclick="markDone(${j}, ${i})"><i
                        class="material-icons">done</i>
                    </a>
                </span>
            </li>`
        }
        document.getElementById("queueContent").innerHTML = output
    }
}

// Anonymous function that runs when the web page is loaded.
window.onload = function () {
    // Call function liveClock() and sets an interval to call the function every 1000 millisecons.
    liveClock()
    setInterval(liveClock, 1000)
    // Call function displayQueueStatus to update the main page with the session instance.
    displayQueueStatus(consultSession)
}