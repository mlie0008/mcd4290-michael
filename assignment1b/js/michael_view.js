"use strict";

// michael_view.js runs when michael_view.html is opened and calls the method getStudent() from the session class in order to find the student data object and display it to the web page.

// Loads the student and subqueue data objects to the appropriate variables.
let studentIndex = localStorage.getItem(STUDENT_INDEX_KEY)
let queueIndex = localStorage.getItem(STUDENT_QUEUE_KEY)

// Call method getStudent with the student and subqueue index as parameters to return the student data object.
let studentData = consultSession.getStudent(studentIndex, queueIndex)

// Assignes the HTML elements with the appropriate attributes.
document.getElementById("fullName").innerHTML = studentData._fullName
document.getElementById("studentId").innerHTML = studentData._studentId
document.getElementById("problem").innerHTML = studentData._problem