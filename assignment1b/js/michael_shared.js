"use strict";

// michael_shared.js contains the classes and methods, as well as all the functions that start when the web page is opened.

// Keys for localStorage
const STUDENT_INDEX_KEY = "studentIndex";
const STUDENT_QUEUE_KEY = "queueIndex";
const APP_DATA_KEY = "consultationAppData";

class Student {
    // Takes the student's full name, ID, and problem and assigns them to the appropriate attributes.
    constructor(fullName, studentId, problem) {
        this._fullName = fullName
        this._studentId = studentId
        this._problem = problem
    }

    get fullName() {
        return this._fullName
    }
    get studentId() {
        return this._studentId
    }
    get problem() {
        return this._problem
    }

    // Method fromData() assigns the student data object to the appropriate attributes.
    fromData(student) {
        this._fullName = student._fullName
        this._studentId = student._studentId
        this._problem = student._problem
    }
}

class Session {
    // Creates a new start time using the Date() method and a new queue attribute which holds the two sub queues.
    constructor() {
        this._startTime = new Date()
        this._queue = []
    }

    get startTime() {
        return this._startTime
    }
    get queue() {
        return this._queue
    }

    // Method addSubQueue() pushes two new sub queues to the main queue.
    addSubQueue() {
        this._queue.push([], [])
    }

    // Method addStudent() takes the student's full name, ID, and problem and creates a new student instance with those parameters.
    addStudent(fullName, studentId, problem) {
        let student = new Student (fullName, studentId, problem)

        // If else statements to determine which sub queue the student instance gets added to.
        if (this._queue[0].length < this._queue[1].length) {
            this._queue[0].push(student)
        } else if (this._queue[0] > this._queue[1]) {
            this._queue[1].push(student)
        } else {
            this._queue[0].push(student)
        }
    }

    // Method removeStudent() takes the student's and respective subqueue index and removes them from the subqueue.
    removeStudent(studentIndex, queueIndex) {
        this._queue[queueIndex].splice(studentIndex, 1)
    }

    // Method getStudent() takes the student's and respective subqueue index and returns the data object of the student.
    getStudent(studentIndex, queueIndex) {
        let subQueue = this._queue[queueIndex]
        return subQueue[studentIndex]
    }

    // Method fromData() takes the session instance from local storage and pushes two new subqueues to the queue.
    fromData(session) {
        this._queue.push([], [])

        // Nested for loop to iterate over the student data objects taken from local storage and pushes them to their respective subqueues.
        for (let i = 0; i < session._queue.length; i++) {
            let subQueue = session._queue[i]
            for (let j = 0; j < subQueue.length; j++) {
                let newStudent = new Student()
                newStudent.fromData(subQueue[j])
                this._queue[i].push(newStudent)
            }
        }
    }
}

// Function checkLocalStorageDataExist takes the app data key to check if there is existing data.
function checkLocalStorageDataExist(key) {
    // Returns true if data exists.
    if (localStorage.getItem(key)) {
        return true;
    }
    // Returns false if data doesn't exist.
    return false;
}

// Function updateLocalStorageData takes the app data key and respective session data. Saves the session data to local storage and sets the key.
function updateLocalStorageData(key, data) {
    let jsonData = JSON.stringify(data);
    localStorage.setItem(key, jsonData);
}

// Function getLocalStorageData takes the app data key and loads the existing session data.
function getLocalStorageData(key) {
    let jsonData = localStorage.getItem(key);
    let data = jsonData;
    // Attempt to load the session data.
    try {
        data = JSON.parse(jsonData);
        // Logs an error if the task fails.
    } catch(e) {
        console.error(e);
        // Returns the data if the task succeeds.
    } finally {
        return data;
    }
}

// Code that runs when the file is loaded.
let consultSession = new Session();
// Creates a new session instance, load from local storage if data exists.
if (checkLocalStorageDataExist(APP_DATA_KEY)) {
    let data = getLocalStorageData(APP_DATA_KEY);
    consultSession.fromData(data);
    // Creates two new subqueues if the data doesn't exist and saves to the local storage.
} else {
    consultSession.addSubQueue(); 
    updateLocalStorageData(APP_DATA_KEY, consultSession);
}